(function () {
    'use strict';

    angular
        .module('App')
        .controller('ContactsController', ContactsController);

    ContactsController.$inject = ['$scope','$ionicLoading', 'firebaseService', '$state', '$rootScope', '_', '$ionicSideMenuDelegate'];
    function ContactsController($scope,$ionicLoading, firebaseService, $state, $rootScope, _, $ionicSideMenuDelegate) {

        $scope.navHeader ="img/header_contato.jpg";
        $scope.navIconLeft ="img/chat/icon_left.png";
        $scope.navIconRight = "img/chat/icon_right.png";
        $scope.users = {};
        $scope.group = $rootScope.user.clients[$rootScope.currentGroup];

        var groupUsers = $rootScope.user.clients[$rootScope.currentGroup].users;

        $scope.showUser = function (user) {
                return $rootScope.user.guid !=user.$id && $rootScope.profileAll[user.profile].chat;
        };
        
        $scope.sideMenu = function () {
            $ionicSideMenuDelegate.toggleLeft();
        };
        var ids =_.keys(groupUsers);
       
        $ionicLoading.show();
        firebaseService.getUsersbyClient(ids).then(function (result) {
            $ionicLoading.hide();
            $scope.users = result;
        });


    }
})();