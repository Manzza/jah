(function () {
    'use strict';

    angular
        .module('App')
        .controller('RetifyController', RetifyController);
    RetifyController.$inject = ['$scope', 'retifyServices', '$rootScope'];
    function RetifyController($scope, retifyServices, $rootScope) {
        $scope.corregirOcorrencia = function (txt) {
            console.log(txt);
            if (txt) {
                retifyServices.sendRetify($scope.id, txt, $rootScope.user.guid);
                $scope.myModal.remove();
            } else {
                alert("Por favor nos informe o motivo.");
            }
        };
    }
})();