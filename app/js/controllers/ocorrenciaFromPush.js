(function () {
  'use strict';

  angular
    .module('App')
    .controller('ocorrenciaFromPushController', function ($scope,
      $rootScope,
      $ionicSlideBoxDelegate,
      $window,
      $ionicGesture,
      fb,
      $ionicLoading,
      $stateParams,
      $ionicHistory,
      pushServices,
      retifyServices,
      firebaseService,
      $ionicSideMenuDelegate,
      $ionicModal,
      $state,
      $cordovaFileTransfer) {

      $ionicLoading.show();
      var message;
      var title;
      var client;
      var id;

      firebaseService.getOcorrenciaById($stateParams.section.$id + '/' + $stateParams.id).then(function (ocorrencia) {
        $scope.ocorrencia = ocorrencia;
        $scope.profile = $rootScope.user.profile;

        // Header e banner setup
        $scope.navBarClass = "img/" + $stateParams.section.$id + "/header.jpg";
        $scope.navTitle = $stateParams.section.name;
        $scope.navLogo = "img/" + $stateParams.section.$id + "/logo.png";
        $scope.category = $stateParams.section;
        $scope.tabColor = $stateParams.section.logoColor;
        $scope.tag = $scope.ocorrencia.tagList;

        //Variaveis para envio de notificação
        message = $scope.ocorrencia.titOcor;
        title = $scope.ocorrencia.descAssu;
        client = $rootScope.user.clients[$rootScope.currentGroup].$id;
        id = $scope.ocorrencia.$id;
        $scope.profile = $rootScope.user.profile;
        $ionicLoading.hide();
      });

      $scope.notificarCliente = function () {
        $ionicLoading.show();
        pushServices.sendPush(message, title, $stateParams.section.$id, id, client).then(function () {
          $ionicLoading.hide();
        });
      };
      $scope.sideMenu = function () {
        $ionicSideMenuDelegate.toggleLeft();
      };

      $scope.openRetifyModal = function () {
        openModal('./templates/modals/retify.html');
      };

      var openModal = function (templateUrl) {
        return $ionicModal.fromTemplateUrl(templateUrl, {
          scope: $scope,
          animation: 'slide-in-up',
        }).then(function (modal) {
          modal.show();
          $scope.myModal = modal;
          $scope.id = id;

        });
      };

      $scope.voltar = function () {
        $state.go('app.home');
      };

      $scope.innapBrowser = function (value) {
        if (value.indexOf('dropbox') > -1) {
          $ionicLoading.show({ template: '<ion-spinner></ion-spinner></br>Baixando arquivo...' });
          var temp = value.split('/');
          var length = temp.length;
          var name = temp[length - 1].replace(/%20/g, '');
          $cordovaFileTransfer.download(value.replace('dl=0', 'dl=1'), cordova.file.cacheDirectory + '/' + name, {}, true).then(function (result) {
            $ionicLoading.hide();
            window.resolveLocalFileSystemURL(result.nativeURL, function (resultFile) {
              resultFile.file(function (file) {
                cordova.plugins.fileOpener2.open(result.nativeURL, file.type, {
                  error: function (error) {
                    console.error(error);
                    // alert(error.message);
                  }
                })
              }, function (error) {
                console.error(error);
              });
            }, function (error) {
              console.error(error);
            });
          }, function (error) {
            $ionicLoading.hide();
            console.log('Error', error);
          }, function (progress) {
            // console.log(progress);
          });
        } else {
          $window.open(value, '_blank');
        }
      };

    });
})();
