(function () {
	'use strict';

	angular
		.module('App')
		.controller('ChatController', ChatController);

	ChatController.$inject = ['$scope', '$rootScope', '$state',
		'$stateParams', 'MockService','pushServices',
		'$ionicPopup', '$ionicScrollDelegate', '$timeout', '$interval',
		'$ionicActionSheet', '$filter', '$ionicModal', 'firebaseService', 'amMoment', '$ionicSideMenuDelegate'];
	function ChatController($scope, $rootScope, $state, $stateParams, MockService,pushServices,
		$ionicPopup, $ionicScrollDelegate, $timeout, $interval, $ionicActionSheet,
		$filter, $ionicModal, firebaseService, amMoment, $ionicSideMenuDelegate) {

		amMoment.changeLocale('pt-br');
		$scope.isSuporte = false;
		if($stateParams.groupId && $stateParams.groupId.name=='Suporte'){
			$scope.isSuporte = true;
		}
		$scope.navHeader ="img/header_contato.jpg";
        $scope.navIconLeft ="img/"+($scope.isSuporte?'suporte':'chat')+"/icon_left.png";
        $scope.navIconRight = "img/"+($scope.isSuporte?'suporte':'chat')+"/icon_right.png";


		$scope.sideMenu = function () {
			$ionicSideMenuDelegate.toggleLeft();
		};
		var roomId = getRoomId();


		firebaseService.getRoom(roomId).then(function (result) {
			if (result) {
				getMessages();
			} else {
				firebaseService.createRoom(roomId).then(function (success) {
				}, function (error) {
					console.log(error);
				});
			}
		});
		$scope.user = $rootScope.user;

		var messageCheckTimer;

		var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
		var footerBar;
		var scroller;
		var txtInput;

		$scope.$on('$ionicView.enter', function () {
			$timeout(function () {
				cordova.plugins.Keyboard.disableScroll(false);				
				footerBar = document.body.querySelector('.chatView .bar-footer');
				scroller = document.body.querySelector('.chatView .scroll-content');
				txtInput = angular.element(footerBar.querySelector('textarea'));
			}, 0);

			messageCheckTimer = $interval(function () {
				getMessages();
			}, 20000);
		});

		$scope.$on('$ionicView.leave', function () {
			cordova.plugins.Keyboard.disableScroll(true);			
			// Make sure that the interval is destroyed
			if (angular.isDefined(messageCheckTimer)) {
				$interval.cancel(messageCheckTimer);
				messageCheckTimer = undefined;
			}
		});

		function getMessages() {
			// the service is mock but you would probably pass the toUser's GUID here
			firebaseService.getRoomMessageById(roomId).then(function (data) {
				$scope.doneLoading = true;
				$scope.messages = data;
			});
		}
		function getRoomId() {
			if (!$stateParams.groupId) {
				var room = [$rootScope.user.guid, $stateParams.toUser.$id].sort();
				$scope.viewname = $stateParams.toUser.nickname;
				return room[0] + room[1];
			} else {
				$scope.viewname = $stateParams.groupId.name;
				return $stateParams.groupId.guid;
			}
		}

		//#########     TO-DO        ####################################
		//FOTO
		var lastPhoto = 'img/donut.png';

		$scope.sendPhoto = function () {
			$ionicActionSheet.show({
				buttons: [
					{ text: 'CAMERA' },
					{ text: 'GALERIA' }
				],
				titleText: 'Upload image',
				cancelText: 'Cancel',
				buttonClicked: function (index) {

					var message = {
						toId: $scope.toUser._id,
						photo: lastPhoto
					};
					lastPhoto = lastPhoto === 'img/donut.png' ? 'img/woho.png' : 'img/donut.png';
					addMessage(message);

					$timeout(function () {
						var message = MockService.getMockMessage();
						message.date = new Date();
						$scope.messages.push(message);
					}, 2000);
					return true;
				}
			});
		};
		var addMessage = function (message) {
			message._id = new Date().getTime(); // :~)
			message.date = new Date();
			message.username = $scope.user.username;
			message.userId = $scope.user._id;
			message.pic = $scope.user.picture;
			$scope.messages.push(message);
		};
		//#########     TO-DO END       ####################################

		$scope.sendMessage = function (sendMessageForm) {
			var message = {};
			message.text = $scope.input.message;
			message.userId = $rootScope.user.guid;
			message.thumb = $rootScope.user.thumb;
			message.name = $rootScope.user.name;
			message.read = false;
			$scope.input.message = '';
			firebaseService.sendRoomMessageById(roomId, message);
			if($scope.isSuporte){
			pushServices.sendSuportPush($rootScope.user,$rootScope.currentGroup);
			}else{
			pushServices.sendChatPush($stateParams.toUser.email, $rootScope.user,$rootScope.currentGroup);
			}
			$timeout(function () {
				keepKeyboardOpen();
			}, 0);

		};

		// this keeps the keyboard open on a device only after sending a message, it is non obtrusive
		function keepKeyboardOpen() {
			txtInput.one('blur', function () {
				txtInput[0].focus();
			});
		}
		$scope.refreshScroll = function (scrollBottom, timeout) {
			$timeout(function () {
				scrollBottom = scrollBottom || $scope.scrollDown;
				viewScroll.resize();
				if (scrollBottom) {
					viewScroll.scrollBottom(true);
				}
				$scope.checkScroll();
			}, timeout || 1000);
		};
		$scope.scrollDown = true;
		$scope.checkScroll = function () {
			$timeout(function () {
				var currentTop = viewScroll.getScrollPosition().top;
				var maxScrollableDistanceFromTop = viewScroll.getScrollView().__maxScrollTop;
				$scope.scrollDown = (currentTop >= maxScrollableDistanceFromTop);
				$scope.$apply();
			}, 0);
			return true;
		};

		var openModal = function (templateUrl) {
			return $ionicModal.fromTemplateUrl(templateUrl, {
				scope: $scope,
				animation: 'slide-in-up',
				backdropClickToClose: false
			}).then(function (modal) {
				modal.show();
				$scope.modal = modal;
			});
		};
		$scope.closeModal = function () {
			$scope.modal.remove();
		};

		$scope.photoBrowser = function (message) {
			var messages = $filter('orderBy')($filter('filter')($scope.messages, { photo: '' }), 'date');
			$scope.activeSlide = messages.indexOf(message);
			$scope.allImages = messages.map(function (message) {
				return message.photo;
			});

			openModal('templates/modals/fullscreenImages.html');
		};

		$scope.onMessageHold = function (e, itemIndex, message) {
			$ionicActionSheet.show({
				buttons: [{
					text: 'Copy Text'
				}, {
					text: 'Delete Message'
				}],
				buttonClicked: function (index) {
					switch (index) {
						case 0: // Copy Text
							//cordova.plugins.clipboard.copy(message.text);

							break;
						case 1: // Delete
							// no server side secrets here :~)
							$scope.messages.splice(itemIndex, 1);
							$timeout(function () {
								viewScroll.resize();
							}, 0);

							break;
					}

					return true;
				}
			});
		};


		$scope.$on('elastic:resize', function (event, element, oldHeight, newHeight) {
			if (!footerBar) return;

			var newFooterHeight = newHeight + 10;
			newFooterHeight = (newFooterHeight > 44) ? newFooterHeight : 44;

			footerBar.style.height = newFooterHeight + 'px';
			scroller.style.bottom = newFooterHeight + 'px';
		});

	}
})();