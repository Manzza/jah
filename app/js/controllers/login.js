(function () {
    'use strict';

    angular
        .module('App')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$scope', '$ionicHistory', '$ionicModal', '$rootScope', 'firebaseService', '$state', '$ionicLoading'];
    function LoginController($scope, $ionicHistory, $ionicModal, $rootScope, firebaseService, $state, $ionicLoading) {
        $scope.user = {};
        $scope.showForm = true;

        $scope.emailLogin = function (user, pwd) {
            if (!user) {
                alert('Digite um endereço de e-mail válido');
            } else if (!pwd) {
                alert('Digite sua senha');
            } else {
                $ionicLoading.show();
                firebaseService.emailLogin(user, pwd).then(function (result) {
                    $ionicLoading.hide();
                    if ($rootScope.currentGroup) {
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go('app.home');
                    } else {
                        openModal('./templates/modals/changeClient.html');
                    }
                }).catch(function (error) {
                    $scope.showForm = true;
                    $ionicLoading.hide();
                    alert(loginErrorHandler(error));
                    console.log(error);
                });
            }
        };

        function loginErrorHandler(error) {
            var codes = {
                invalidemail: 'E-mail inválido',
                userdisabled: 'Usuário desabilitado',
                usernotfound: 'E-mail não cadastrado',
                wrongpassword: 'Senha incorreta'
            };
            return codes[error];
        }

        var openModal = function (templateUrl) {
            $ionicModal.fromTemplateUrl(templateUrl, {
                scope: $scope,
                animation: 'slide-in-up',
            }).then(function (modal) {
                modal.show();
                $scope.myModal = modal;
            });
        };

        if (window.localStorage.getItem('emailJa')) {
            if (!$rootScope.fromPush) {
                $scope.showForm = true;
                $ionicLoading.show();
                $scope.emailLogin(
                    window.localStorage.getItem('emailJa'),
                    window.localStorage.getItem('passwordJa')
                );
            }
        } else {
            $scope.showForm = true;
        }
    }
})();