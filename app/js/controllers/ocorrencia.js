(function () {
  'use strict';

  angular
    .module('App')
    .controller('ocorrenciaController', function ($scope,
      $rootScope,
      $ionicSlideBoxDelegate,
      $window,
      $ionicGesture,
      fb,
      $ionicLoading,
      $stateParams,
      $ionicHistory,
      pushServices,
      retifyServices,
      $ionicSideMenuDelegate,
      $ionicModal,
      $state,
      $cordovaFileTransfer) {

      // Header e banner setup  
      $scope.navBarClass = "img/" + $stateParams.section.$id + "/header.jpg";
      $scope.navTitle = $stateParams.section.name;
      $scope.navLogo = "img/" + $stateParams.section.$id + "/logo.png";
      $scope.category = $stateParams.section;
      $scope.ocorrencias = $rootScope.ocorrenciasSeparadas[$stateParams.section.$id];
      $scope.active = $stateParams.active;
      $scope.tag = $scope.ocorrencias[$scope.active].tagList;
      $scope.tabColor = $stateParams.section.logoColor;

      //Variaveis para e  nvio de notificação
      var message = $scope.ocorrencias[$scope.active].titOcor;
      var id = $scope.ocorrencias[$scope.active].$id;
      var title = $scope.ocorrencias[$scope.active].descAssu;
      var client = $rootScope.user.clients[$rootScope.currentGroup].$id;
      $scope.profile = $rootScope.user.profile;

      $scope.slideHasChanged = function ($index) {
        $scope.tag = $scope.ocorrencias[$index].tagList;
        message = $scope.ocorrencias[$index].titOcor;
        title = $scope.ocorrencias[$index].descAssu;
        id = $scope.ocorrencias[$index].$id;
        console.log($scope.ocorrencias[$index])
      };

      $scope.teste = function (data) {
        var temp = data.split(' ')[1].split(':');
        return temp[0] + ':' + temp[1];
      }

      $scope.notificarCliente = function () {
        $ionicLoading.show();
        pushServices.sendPush(message, title, $stateParams.section.$id, id, client).then(function () {
          $ionicLoading.hide();
        });
      };

      $scope.sideMenu = function () {
        $ionicSideMenuDelegate.toggleLeft();
      };

      $scope.openRetifyModal = function () {
        openModal('./templates/modals/retify.html');
      };

      var openModal = function (templateUrl) {
        return $ionicModal.fromTemplateUrl(templateUrl, {
          scope: $scope,
          animation: 'slide-in-up',
        }).then(function (modal) {
          modal.show();
          $scope.myModal = modal;
          $scope.id = id;
        });
      };

      $scope.voltar = function () {
        $state.go('app.home', { active: $stateParams.index });
      };

      $scope.log = function (log) {
        console.log(log);
      }

      $scope.innapBrowser = function (value) {
        if (value.indexOf('dropbox') > -1) {
          $ionicLoading.show({ template: '<ion-spinner></ion-spinner></br>Baixando arquivo...' });
          var temp = value.split('/');
          var length = temp.length;
          var name = temp[length - 1].replace(/%20/g, '');
          $cordovaFileTransfer.download(value.replace('dl=0', 'dl=1'), cordova.file.dataDirectory + '/' + name, {}, true).then(function (result) {
            $ionicLoading.hide();
            window.resolveLocalFileSystemURL(result.nativeURL, function (resultFile) {
              resultFile.file(function (file) {
                cordova.plugins.fileOpener2.open(result.nativeURL, file.type, {
                  error: function (error) {
                    console.error(error);
                    // alert(error.message);
                  }
                })
              }, function (error) {
                console.error(error);
              });
            }, function (error) {
              console.error(error);
            });
          }, function (error) {
            $ionicLoading.hide();
            console.log('Error', error);
          }, function (progress) {
            // console.log(progress);
          });
        } else {
          $window.open(value, '_blank');
        }
      };

    });
})();
