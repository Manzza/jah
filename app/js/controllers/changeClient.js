(function () {
    'use strict';

    angular
        .module('App')
        .controller('ChangeClientsController', ChangeClientsController);
    ChangeClientsController.$inject = ['$scope','$ionicHistory', 'firebaseService', '$state', '$rootScope', '_'];
    function ChangeClientsController($scope,$ionicHistory, firebaseService, $state, $rootScope, _) {

        $scope.clients = $rootScope.user.clients;
        $scope.$on('cloud:push:notification', function (event, data) {
            console.log('wtf');
            $scope.myModal.remove();
        });
        $scope.setClient = function (client) {
            $rootScope.currentGroup = client.$id;
            firebaseService.getClientData().then(function (ret) {
                $rootScope.searchLimit = ret.settings;
                $scope.myModal.remove();
                 $ionicHistory.nextViewOptions({
                        disableBack: true
                    });
                $state.go('app.home', {}, { reload: 'app.home' });

            });

        };
    }
})();