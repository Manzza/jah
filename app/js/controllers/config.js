(function () {
  'use strict';

  angular
    .module('App')
    .controller('ConfigController', ConfigController);

  ConfigController.$inject = ['$scope','$http', '$ionicPopup','$ionicLoading', 'pushService', 'firebaseService', '$state', '$rootScope', '_', '$ionicSideMenuDelegate'];
  function ConfigController($scope,$http, $ionicPopup,$ionicLoading, pushService, firebaseService, $state, $rootScope, _, $ionicSideMenuDelegate) {

    $scope.navHeader = "img/header_contato.jpg";
    $scope.navIconLeft = "img/chat/icon_left.png";
    $scope.navIconRight = "img/chat/icon_right.png";

    $scope.sideMenu = function () {
      $ionicSideMenuDelegate.toggleLeft();
    };

    $scope.resetPassword = function(){
      return $http.post('http://52.67.191.118/resetarSenha', {email:$rootScope.user.email }).then(function (result) {
      return $ionicPopup.alert({
          title: result.data=="Sucesso"?'Email enviado ':'Erro ',
          template: result.data=="Sucesso"?'Um email para alterar a senha foi enviado para o email : '+$rootScope.user.email+'.':'Houve um problema para processar sua solicitação, por favor aguarde e tenta mais tarde ou entre em contato com o suporte. ',
        });
    });
  }

    $scope.logout = function () {
      $ionicPopup.confirm({
        title: 'Sair',
        template: 'Deseja deslogar do aplicativo?',
        cancelText: 'Não',
        okText: 'Sair'
      }).then(function (res) {
        if (res) {
          $rootScope.currentGroup = null;
          $rootScope.user = null;
          window.localStorage.removeItem('emailJa');
          window.localStorage.removeItem('passwordJa');
          $state.go('login');
        }
      })

    };

    $scope.changeSound = function (sound) {
      pushService.setSound(sound);
      firebaseService.setPushSound($rootScope.user.guid, sound);
    };

    $scope.changeVibrate = function (vibrate) {
      pushService.setVibrate(vibrate);
      firebaseService.setPushVibrate($rootScope.user.guid, vibrate);
    };

  }
})();