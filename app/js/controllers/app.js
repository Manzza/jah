(function () {
    'use strict';

    angular
        .module('App')
        .controller('AppController', AppController);

    AppController.$inject = ['$scope', '$ionicPopover', '$rootScope', 'firebaseService', '$state', '$ionicModal'];
    function AppController($scope, $ionicPopover, $rootScope, firebaseService, $state, $ionicModal) {
        firebaseService.myTest();

        $scope.menuItem = 0;
        $scope.changeMenuItem = function (num) {
            $scope.menuItem = num;
        };

        if (!$rootScope.user) {
            $state.go('login');
        };

        $scope.changeGroup = function () {
            openModal('./templates/modals/changeClient.html');
            console.log($rootScope.user)
        };

        $scope.exitApp = function () {
            $rootScope.currentGroup = null;
            $rootScope.user = null;
            window.localStorage.removeItem('emailJa');
            window.localStorage.removeItem('passwordJa');
        };

        var openModal = function (templateUrl) {
            $ionicModal.fromTemplateUrl(templateUrl, {
                scope: $scope,
                animation: 'slide-in-up',
            }).then(function (modal) {
                modal.show();
                $scope.myModal = modal;
            });
        };

        $ionicPopover.fromTemplateUrl('templates/modals/popover.html', {
            scope: $scope
        }).then(function (popover) {
            $scope.popover = popover;
        });

        $scope.openPopover = function ($event) {
            $scope.popover.show($event);
        };

        $scope.$on('$destroy', function () {
            $scope.popover.remove();
        });
    }
})();