(function () {
  'use strict';

  angular
    .module('App')
    .controller('TwitterController', function (
      $scope,
      TwitterREST,
      $rootScope,
      $ionicSideMenuDelegate,
      $stateParams) {
      $scope.header = 'http://ja.w3m.com.br/images/secoes/banner_no_mundo.png';
      var id = $stateParams.linkId;
      TwitterREST.sync(id).then(function (tweets) {
        $scope.tweets = tweets;
      });
      $scope.innapBrowser = function (value) {
        window.open(value, '_blank');
      };

      $scope.sideMenu = function () {
        $ionicSideMenuDelegate.toggleLeft();
      };
    });
})();
