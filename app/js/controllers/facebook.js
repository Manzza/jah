(function () {
  'use strict';

  angular
    .module('App')
    .controller('FacebookController', function (
      $scope,
      $sce,
      $rootScope,
      $ionicSideMenuDelegate,
      $stateParams) {

      $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
      };
      $scope.header='http://ja.w3m.com.br/images/secoes/banner_no_mundo.png';
      var element = document.getElementById("gambeta");
      var src = "http://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2F";
      var id = $stateParams.linkId;
      var tabs = "&tabs=timeline";
      var width = "&width=" + element.offsetWidth;
      var height = "&height=" + element.offsetHeight;
      var resto = "&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=1414494412198189";

      $scope.url = src + id + tabs + width + height + resto;

      $scope.sideMenu = function () {
        $ionicSideMenuDelegate.toggleLeft();
      };

    });
})();
