(function () {
    'use strict';

    angular
        .module('App')
        .controller('HomeController', function (
            $scope,
            $rootScope,
            $ionicSlideBoxDelegate,
            $ionicGesture,
            fb,
            $ionicLoading,
            $state,
            $ionicModal,
            $ionicSideMenuDelegate,
            $filter,
            ionicDatePicker,
            $cordovaToast,
            $stateParams,
            firebaseService,
            $window,
            $ionicPlatform,
            $ionicPopup,
            $ionicHistory) {

            $ionicPlatform.on('resume', function () {
                $scope.$apply(function () {


                    for (var index = 0; index < backup.length; index++) {
                        backup[index].off();
                    }
                    var backup = [];
                    if ($rootScope.filterOn) {
                        $scope.ocorrenciasSeparadas = {};
                        $scope.assunto = $rootScope.filter.assunto
                        $scope.sentimentoFilter = $rootScope.filter.sentimentoFilter;
                        $scope.tagsFilter = $rootScope.filter.tagsFilter;
                        $scope.data_init = $rootScope.filter.data_init;
                        $scope.data_end = $rootScope.filter.data_end;
                        $scope.filtrar();
                    } else {
                        $scope.ocorrenciasSeparadas = {};
                        initFirebase();
                    }
                });
            });

            var backButton = 0;
            $rootScope.menuItem = 0;
            $ionicPlatform.registerBackButtonAction(function (event) {
                console.log($ionicHistory.viewHistory());
                if ($ionicHistory.currentView().stateName === "app.home") {
                    $ionicPopup.confirm({
                        title: 'Alerta',
                        template: 'Deseja sair do aplicativo?',
                        cancelText: 'Não',
                        okText: 'Sair'
                    }).then(function (res) {
                        if (res) {
                            ionic.Platform.exitApp();
                        }
                    })
                } else {
                    if ($ionicHistory.viewHistory().backView !== null) {
                        $ionicHistory.goBack();
                    } else {
                        $state.go('app.home');
                    }
                }
            }, 100);

            $scope.client = $rootScope.user.clients[$rootScope.currentGroup];
            $scope.limit = $rootScope.searchLimit;

            var backup = [];
            var ctrl = 0;
            if (!$rootScope.filterOn) {
                initFirebase();
            }

            $scope.criteria = '';

            $scope.$watch('criteria', function (newValue, oldValue) {
                console.log('newValue', newValue);
                console.log('oldValue', oldValue);
            }, true);

            function initFirebase() {
                ctrl = -1;
                $scope.ocorrenciasSeparadas = {};
                var start = Math.floor(new Date().setHours(0, -new Date().getTimezoneOffset(), 0) / 1000);
                var end = Math.floor(new Date().setHours(23, 59 + (-new Date().getTimezoneOffset()), 0) / 1000);
                angular.forEach($scope.client.sections, function (section) {
                    ctrl++;
                    backup.push(fb.child('ocorrencias/' + section.$id).orderByChild("timestamp").startAt(start).endAt(end));
                    $scope.ocorrenciasSeparadas[section.$id] = [];
                    backup[ctrl].on("child_added", function (snapshot) {
                        var temp = snapshot.val();
                        temp.$id = snapshot.key;
                        $scope.ocorrenciasSeparadas[section.$id].push(temp);
                        $scope.ocorrenciasSeparadas[section.$id] = _.sortBy($scope.ocorrenciasSeparadas[section.$id], function (ocor) {
                            return -1 * ocor.timestamp;
                        });
                    });
                });


            }

            $scope.active = 0;
            init();

            $scope.gotoSlide = function (index) {
                $ionicSlideBoxDelegate.slide(index);
            };

            function init() {
                $scope.navBarClass = "img/" + $scope.client.sections[$stateParams.active].$id + "/header.jpg";
                $scope.navTitle = $scope.client.sections[$stateParams.active].name;
                $scope.tabColor = $scope.client.sections[$stateParams.active].logoColor;
                $scope.active = $stateParams.active;

            }

            $scope.mudarAba = function (tab) {
                $scope.active = tab;

                $scope.navBarClass = "img/" + $scope.client.sections[tab].$id + "/header.jpg";
                $scope.navTitle = $scope.client.sections[tab].name;
                $scope.tabColor = $scope.client.sections[tab].logoColor;
                if ($scope.client.sections[tab].name === "Correios em Foco") {
                    $scope.navTitle = '';
                }

            };

            $scope.irParaDetalhe = function (index, active, ocorrencia) {
                $rootScope.ocorrenciasSeparadas = $scope.ocorrenciasSeparadas;
                $state.go('app.ocorrencia', { section: $scope.client.sections[index], active: active, index: index });
                // if (index === 0) {
                //   $state.go('app.ocorrenciaFromPush', { id: ocorrencia.guid, section: $scope.client.sections[index] });
                // } else {
                // }
            };

            $scope.sideMenu = function () {
                $ionicSideMenuDelegate.toggleLeft();
            };

            $scope.openFilter = function () {
                openModal('./templates/modals/filter.html');
            };

            var openModal = function (templateUrl) {
                return $ionicModal.fromTemplateUrl(templateUrl, {
                    scope: $scope,
                    animation: 'slide-in-up',
                }).then(function (modal) {
                    modal.show();
                    $scope.myModal = modal;
                });
            };

            ////////////////////////////////////////////////  
            /////               FILTER                //////  
            ////////////////////////////////////////////////  

            $scope.sentimentoFilter = '';
            $scope.data_init = '';
            $scope.data_end = '';

            $scope.ativarSentimento = function (sentimento) {
                if ($scope.sentimentoFilter === sentimento) {
                    $scope.sentimentoFilter = '';
                } else {
                    $scope.sentimentoFilter = sentimento;
                }
            };

            var perido1 = {
                callback: function (val) {  //Mandatory
                    $scope.dataInit = $filter('date')(new Date(val), "dd/MM/yy");
                    $scope.data_init = new Date(val).setHours(0, 0 + (-new Date().getTimezoneOffset()), 0);

                }
            };

            var perido2 = {
                callback: function (val) {  //Mandatory
                    $scope.dataEnd = $filter('date')(new Date(val), "dd/MM/yy");
                    $scope.data_end = new Date(val).setHours(23, 59 + (-new Date().getTimezoneOffset()), 0);
                }
            };

            $scope.openDatePicker = function (index) {
                ionicDatePicker.openDatePicker(index === 1 ? perido1 : perido2);
            };

            $scope.filtrar = function () {
                $scope.ocorrenciasSeparadas = {};
                if ($scope.data_init !== '' && $scope.data_end === '') {
                    alert('Selecione a data final');
                } else if ($scope.data_end !== '' && $scope.data_init === '') {
                    alert('Selecione a data inicial');
                } else {
                    if ($scope.assunto === '' && $scope.sentimentoFilter === '' && $scope.tagsFilter === '' && $scope.data_init === '' && $scope.data_end === '') {
                        $rootScope.filterOn = false;
                    } else {
                        $rootScope.filterOn = true;
                        $rootScope.filter = {};
                        $rootScope.filter.assunto = $scope.assunto;
                        $rootScope.filter.sentimentoFilter = $scope.sentimentoFilter;
                        $rootScope.filter.tagsFilter = $scope.tagsFilter;
                        $rootScope.filter.data_init = $scope.data_init;
                        $rootScope.filter.data_end = $scope.data_end;

                    }
                    $scope.assuntoFilter = this.assuntoFilter;
                    $scope.tagsFilter = this.tagsFilter;

                    if ($scope.data_init !== '' && $scope.data_end !== '') {
                        $ionicLoading.show();
                        var filterStart = Math.floor($scope.data_init / 1000);
                        var filterEnd = Math.floor($scope.data_end / 1000);
                        angular.forEach($scope.client.sections, function (section) {
                            $scope.ocorrenciasSeparadas[section.$id] = [];
                            var count = 0;
                            fb.child('ocorrencias/' + section.$id).orderByChild("timestamp").startAt(filterStart).endAt(filterEnd).limitToFirst(20).once("value").then(function (snapshot) {
                                for (var res in snapshot.val()) {
                                    var temp = snapshot.val()[res];
                                    temp.$id = res;
                                    $scope.ocorrenciasSeparadas[section.$id].push(temp);
                                    $scope.ocorrenciasSeparadas[section.$id] = _.sortBy($scope.ocorrenciasSeparadas[section.$id], function (ocor) {
                                        return -1 * ocor.timestamp;
                                    });
                                }
                            });
                        });
                        setTimeout(function () {
                            $scope.$apply();
                            $ionicLoading.hide();
                            $scope.myModal.remove();
                        }, 500);
                    } else {
                        $scope.myModal.remove();
                    }
                }
            };

            $scope.cleanFilter = function () {
                $rootScope.filterOn = false;
                for (var index = 0; index < backup.length; index++) {
                    backup[index].off();
                }
                initFirebase();
                $scope.myModal.remove();
            };

        });
})();
