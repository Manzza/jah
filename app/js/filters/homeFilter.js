(function () {
    'use strict';

    angular
        .module('App')
        .filter('homeFilter', function (amMoment) {
            return function (item, dataInicio, dataFinal) {
                var filtered = [];
                var dataInicioParts = dataInicio.split('/');
                var datInicio = new Date(dataInicioParts[2], dataInicioParts[1] - 1, dataInicioParts[0]);
                var dataFinalParts = dataFinal.split('/');
                var datFinal = new Date(dataFinalParts[2], dataFinalParts[1] - 1, dataFinalParts[0]);
                if (dataInicio !== '' && dataFinal !== '') {
                    for (var index = 0; index < item.length; index++) {
                        var ocorDataParts = item[index].datOcor.split(' ')[0].split('/');
                        var ocorenciaData = new Date(ocorDataParts[2], ocorDataParts[1] - 1, ocorDataParts[0]);
                        if (ocorenciaData >= datInicio && ocorenciaData <= datFinal) {
                            filtered.push(item[index]);
                        }
                    }
                    return filtered;
                } else {
                    return item;
                }
            };

        }).filter('htmlBind', htmlBind);

    function htmlBind() {
        return function (data) {
            if (!data) return data;
            return data.replace(/\n?/g, '').replace(/\r?/g, '');
        };
    }

})();
