var app = angular.module('filters', []);

app.filter('formatOcorrenciaText', function () {
    /*
        Parses tweet text for links, hashes etc.
    */
    return function (ocorr) {
        OcorrFunc = {
            link: function (ocorr) {
                return ocorr.replace(/\b((https*\:\/\/).*?)">\b(.[^<]*)/g, function (link, m1, m2, m3, m4) {
                    return ' " ng-click="innapBrowser(\'' + m1 + '\')">' + m3 + '</a>';
                });
            }
        };
        return OcorrFunc.link(ocorr);
    };
});

app.filter('formatTwitterText', function () {
    /*
        Parses tweet text for links, hashes etc.
    */
    return function (tweet) {
        //console.log(tweet);
        TweetFunc = {
            link: function (tweet) {
                return tweet.replace(/\b(((https*\:\/\/)|www\.)[^\"\']+?)(([!?,.\)]+)?(\s|$))/g, function (link, m1, m2, m3, m4) {
                    var http = m2.match(/w/) ? 'http://' : '';
                    return '<a class="twtr-hyperlink" ng-click="innapBrowser(\'' + http + m1 + '\')">' + ((m1.length > 25) ? m1.substr(0, 24) + '...' : 'rola') + '</a>';
                });
            },
            at: function (tweet) {
                return tweet.replace(/\B[@＠]([a-zA-Z0-9_]{1,20})/g, function (m, username) {
                    return '<a class="twtr-atreply" ng-click="innapBrowser(\'http://twitter.com/intent/user?screen_name=' + username + '\')">@' + username + '</a>';
                });
            },
            list: function (tweet) {
                return tweet.replace(/\B[@＠]([a-zA-Z0-9_]{1,20}\/\w+)/g, function (m, userlist) {
                    return '<a class="twtr-atreply" ng-click="innapBrowser(\'http://twitter.com/' + userlist + '\')">@' + userlist + '</a>';
                });
            },
            hash: function (tweet) {
                return tweet.replace(/(^|\s+)#(\w+)/gi, function (m, before, hash) {
                    return before + '<a target="_blank" class="twtr-hashtag" ng-click="innapBrowser(\'http://twitter.com/search?q=%23' + hash + '\')">#' + hash + '</a>';
                });
            }
        };

        return TweetFunc.hash(TweetFunc.at(TweetFunc.list(TweetFunc.link(tweet))));
    };
});