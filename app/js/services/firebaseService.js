(function () {
  'use strict';

  angular
    .module('App')
    .factory('firebaseService', firebaseService);

  firebaseService.$inject = ['$window', 'fb', '$cordovaToast', '_', '$ionicPush', '$q', '$ionicPopup', '$firebaseArray', '$firebaseObject', '$ionicLoading', '$firebaseAuth', '$rootScope'];

  /* @ngInject */
  function firebaseService($window, fb, $cordovaToast, _, $ionicPush, $q, $ionicPopup, $firebaseArray, $firebaseObject, $ionicLoading, $firebaseAuth, $rootScope) {
    var service = {
      emailLogin: emailLogin,
      getUserById: getUserById,
      getRoom: getRoom,
      createRoom: createRoom,
      getRoomMessageById: getRoomMessageById,
      sendRoomMessageById: sendRoomMessageById,
      getUsersbyClient: getUsersbyClient,
      getClientData: getClientData,
      myTest: myTest,
      resetPassword: resetPassword,
      getOcorrenciasBetweenTime: getOcorrenciasBetweenTime,
      getOcorrenciaById: getOcorrenciaById,
      getSectionById: getSectionById,
      getSuportPush: getSuportPush,
      setPushSound:setPushSound,
      setPushVibrate:setPushVibrate
    };
    return service;

    //--------------------------------------------------------
    function emailLogin(email, password) {
      var q = $q.defer();
      $firebaseAuth().$signInWithEmailAndPassword(email, password).then(function (authData) {
        getUserById(authData.uid).then(initItem).then(populateUser).then(function (userData) {
          $rootScope.user = userData;
          if ($rootScope.currentGroup) {
            getClientData().then(function (ret) {
              $rootScope.searchLimit = ret.settings;
              $rootScope.filterOn = false;
              window.localStorage.setItem('emailJa', email);
              window.localStorage.setItem('passwordJa', password);
              setUser(userData);
              //myTest();
              q.resolve(true);
            });
          } else {
            window.localStorage.setItem('emailJa', email);
            window.localStorage.setItem('passwordJa', password);
            setUser(userData);
            q.resolve(true);
          }
        }, function (error) {
          q.reject();
        });
      }).catch(function (error) {
        q.reject(error.code.split('/')[1].replace(/-/g, ''));
      });
      return q.promise;
    }

    function myTest() {

    }

    function getOcorrencias() {
      var start = Math.floor(new Date().setHours(0, -new Date().getTimezoneOffset(), 0) / 1000);
      var end = Math.floor(new Date().setHours(23, 59 + (-new Date().getTimezoneOffset()), 0) / 1000);
      return $firebaseArray(fb.child('/ocorrencia').orderByChild("timestamp").startAt(start).endAt(end)).$loaded().then(initArray).then(function (snap) {
        var allOcorrencias = snap;
        var sections = $rootScope.user.clients[$rootScope.currentGroup].sections;
        var tempCategory = [];
        var ocorrenciasSeparadas = {};
        for (var index = 0; index < sections.length; index++) {
          ocorrenciasSeparadas[sections[index].$id] = [];
        }
        for (var element in allOcorrencias) {
          for (var i = 0; i < sections.length; i++) {
            if (sections[i].$id === allOcorrencias[element].idSec) {
              ocorrenciasSeparadas[sections[i].$id].push(allOcorrencias[element]);
            }
          }
        }
        return ocorrenciasSeparadas;
      });
    }

    function getOcorrenciasBetweenTime(start, end, limit) {
      start = Math.floor(start / 1000);
      end = Math.floor(end / 1000);
      var counter = 0;
      return $firebaseArray(fb.child('/ocorrencia').orderByChild("timestamp").startAt(start).endAt(end)).$loaded().then(initArray).then(function (snap) {
        var allOcorrencias = snap;
        var sections = $rootScope.user.clients[$rootScope.currentGroup].sections;
        var tempCategory = [];
        var ocorrenciasSeparadas = {};
        for (var index = 0; index < sections.length; index++) {
          ocorrenciasSeparadas[sections[index].$id] = [];
        }
        for (var element in allOcorrencias) {
          for (var i = 0; i < sections.length; i++) {
            if (sections[i].$id === allOcorrencias[element].idSec && counter < limit) {
              counter++;
              ocorrenciasSeparadas[sections[i].$id].push(allOcorrencias[element]);
            }
          }
        }
        return ocorrenciasSeparadas;
      });
    }
    function getOcorrenciaById(id) {
      return $firebaseObject(fb.child('ocorrencias/' + id)).$loaded().then(initItem).then(function (snap) {
        return snap;
      });
    }
    function getSectionById(id) {
      return $firebaseObject(fb.child('/sections/' + id)).$loaded().then(initItem).then(function (snap) {
        return snap;
      });
    }

    function resetPassword(emailAddress) {
      return firebase.auth().sendPasswordResetEmail(emailAddress).then(function () {
        $cordovaToast.show('E-mail enviado!', 'Por favor verificar a sua caixa de mensagens!');

      }, function (error) {
        console.log(error);
        if (error.code === 'auth/user-not-found') {
          $cordovaToast.show('Usuário não cadastrado!', 'Por favor verificar o e-mail digitado ou cadastre-se!');
        }
      });
    }
    function getPush() {
      return $firebaseArray(fb.child('/push/' + $rootScope.currentGroup + '/only/')).$loaded().then(initArray).then(function (snap) {
        var ret = [];
        for (var index = 0; index < snap.length; index++) {
          ret.push(snap[index].$id);
        }
        return ret;
      });
    }

    function getSuportPush() {
      return $firebaseArray(fb.child('/push/' + $rootScope.currentGroup + '/suport/')).$loaded().then(initArray).then(function (snap) {
        var ret = [];
        for (var index = 0; index < snap.length; index++) {
          ret.push(snap[index].$id);
        }
        return ret;
      });
    }


    function registerIonicPush(userData) {
      $ionicPush.register().then(function (t) {
        return $ionicPush.saveToken(t);
      }).then(function (t) {
        var token = t.token;
        fb.child('users/' + userData.$id + '/token').set(token);
        angular.forEach(userData.clients, function (group, key) {
          if (userData.profile.alwalysNotify) {
            fb.child('push/' + key + '/all/' + token).set(true);
          }
          if (userData.profile.onlyByUserNotify) {
            fb.child('push/' + key + '/only/' + token).set(true);
          }
          if (userData.profile.suport) {
            fb.child('push/' + key + '/suport/' + token).set(true);
          }
        });
      });
    }
    function isAvailable() {
      return !!($window.plugins && $window.plugins.OneSignal);
    }
    function setUser(user) {
      if (!isAvailable()) return;
      console.log('Push: sync-user email="%s"', user.email);
      $window.plugins.OneSignal.syncHashedEmail(user.email);

      angular.forEach(user.clients, function (group, key) {
        $window.plugins.OneSignal.sendTag(key + '_alwalysNotify', user.profile.alwalysNotify);
        $window.plugins.OneSignal.sendTag(key + '_onlyByUserNotify', user.profile.onlyByUserNotify);
        $window.plugins.OneSignal.sendTag(key + '_suport', user.profile.suport);
      });
    }

    function populateUser(user) {
      return $q.all([
        getClients(user.clients),
        getProfile(user.profile),
        getProfiles(),
        ]).then(function (results) {
        user.clients = results[0];
        user.profile = results[1];
        return user;
      });
    }
    function getClientData() {
      return $q.all([
        getSettings(),
      ]).then(function (results) {
        var ret = {};
        ret.settings = results[0];
        return ret;
      });
    }

    function getProfile(id) {
      return $firebaseObject(fb.child('profiles/'+id)).$loaded().then(function (ret) {
        return ret;
      });
    }

   

    function setPushSound(id,sound) {
      var ref = fb.child('users/'+id+'/pushSound/').set(sound);
    }

    function setPushVibrate(id,vibrate) {
      var ref = fb.child('users/'+id+'/pushVibrate/').set(vibrate);
    }

    function getProfiles() {
      return $firebaseObject(fb.child('profiles')).$loaded().then(function (ret) {
        $rootScope.profileAll = ret;
        return ret;
      });
    }
    function getSettings() {
      return $firebaseObject(fb.child('settings')).$loaded().then(function (ret) {
        return ret.searchLimit;
      });
    }


    function getClients(clientId) {
      var deffered = $q.defer();
      var result = {};
      angular.forEach(clientId, function (group, key) {
        if (_.size(clientId) == 1)
          $rootScope.currentGroup = group;
        $firebaseObject(fb.child('clients/' + group)).$loaded().then(initItem).then(function (ret) {
          if (ret.status) {
            result[group] = ret;
            result[group].section = [];
            angular.forEach(result[group].sections, function (section, key) {
              $firebaseObject(fb.child('sections/' + key)).$loaded().then(initItem).then(function (sec) {
                result[group].section.push(sec);
              });
            });

            result[group].sections = result[group].section;
          }
        });
      });
      deffered.resolve(result);
      return deffered.promise;
    }

    function initItem(item) {
      return angular.extend({}, item, {
        guid: item.$id
      });
    }

    function initArray(array) {
      return _.map(array, initItem);
    }

    function getAllUsers() {
      var query = fb.child('users');
      return $firebaseArray(query).$loaded();
    }

    function getUsersbyClient(client) {
      var deffered = $q.defer();
      var result = {};
      angular.forEach(client, function (id, key) {
        var query = fb.child('users/' + id);
        $firebaseObject(query).$loaded().then(initItem).then(function (ret) {
          result[id] = ret;
        });
      });
      deffered.resolve(result);
      return deffered.promise;
    }

    function getUserById(id) {
      var query = fb.child('users/' + id);
      return $firebaseObject(query).$loaded();
    }

    function getRoom(id) {
      var ref = firebase.database().ref('rooms/' + id);
      return ref.once("value")
        .then(function (snapshot) {
          return (snapshot.exists());
        });
    }

    function createRoom(id) {
      var query = fb.child('/rooms/');
      var tempObject = $firebaseObject(query);
      return tempObject.$loaded().then(function () {
        tempObject[id] = true;
        return tempObject.$save().then(function () {
          return 'createdRoom';
        });
      });
    }

    function getRoomMessageById(id) {
      var query = fb.child('room-messages/' + id);
      return $firebaseArray(query).$loaded();
    }

    function sendRoomMessageById(id, msg) {
      var ref = fb.child('room-messages/' + id);
      msg.date = firebase.database.ServerValue.TIMESTAMP;
      return ref.push(msg);
    }
  }
})();