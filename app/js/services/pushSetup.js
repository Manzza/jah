(function () {
    'use strict';

    angular
        .module('App')
        .factory('pushService', pushService);

    pushService.$inject = ['$window', '$injector', '$http','firebaseService','$state', '$rootScope', '$ionicLoading'];

    /* @ngInject */
    function pushService($window, $injector, $http,firebaseService,$state, $rootScope, $ionicLoading) {

        var instance = {
            isAvailable: isAvailable,
            setup: setup,
            setUser: setUser,
            setSubscription: setSubscription,
            setSound:setSound,
            setVibrate:setVibrate
        };

        return instance;

        // ****************************************************************

        function isAvailable() {
            return !!($window.plugins && $window.plugins.OneSignal);
        }

        /**
         * Executa sempre que uma notificação é aberta pelo usuário
         * @see https://documentation.onesignal.com/docs/ionic-sdk#section--handlenotificationopened-
         * @param result Conteúdo da notificação
         */
        function handleNotificationOpened(data) {
            console.log('Push: should handle notification opened...data=', data.notification.payload.additionalData);
            var msg = data.notification.payload.additionalData;
            $rootScope.currentGroup = msg.client;
            $ionicLoading.show();
            firebaseService.emailLogin(window.localStorage.getItem('emailJa'),
              window.localStorage.getItem('passwordJa')).then(function (result) {
                switch (msg.type) {
                  case "CHAT":
                    $ionicLoading.hide();
                    firebaseService.getUserById(msg.from).then(function (result) {
                      $state.go('app.chat', { toUser: result });
                    });
                    break;
                  case "SUPORT":
                    $ionicLoading.hide();
                    $state.go('app.chat', {groupId: { name: 'Suporte', guid:msg.client + '_suporte' } });
                    break;
                  case "OCORRENCIA":
                    firebaseService.getSectionById(msg.section).then(function (section) {
                      $ionicLoading.hide();
                      $state.go('app.ocorrenciaFromPush', { id: msg.id, section: section });
                    });
                    break;
                }
              }, function (error) {
                console.log(error);
                $ionicLoading.hide();
    
              });
            
        }

        /**
         * Executa sempre que uma notificação é recebida
         * @see https://documentation.onesignal.com/docs/ionic-sdk#section--handlenotificationreceived-
         * @param data Conteúdo da notificação
         */
        function handleNotificationReceived(data) {
            console.log('Push: should handle notification received...data=', data);
        }

        function setup() {
            if (!isAvailable()) return;

            console.log('Push: Setting up OneSignal for Ja...');
            // TODO: Verificar se o valor esperado para o GoogleAppId é o mesmo do GCM_ID
            $window.plugins.OneSignal
                .startInit("325e2c9a-0337-4e9c-a13a-a4118d915700")
                .handleNotificationOpened(handleNotificationOpened)
                .handleNotificationReceived(handleNotificationReceived)
                .endInit();
        }

        function setUser(user) {
            if (!isAvailable()) return;
            console.log(user);
            console.log('Push: sync-user email="%s"', user.email);
            $window.plugins.OneSignal.syncHashedEmail(user.email);

            angular.forEach(user.clients, function (group, key) {
                $window.plugins.OneSignal.sendTag(key+'_alwalysNotify', user.profile.alwalysNotify);
                $window.plugins.OneSignal.sendTag(key+'_onlyByUserNotify', user.profile.onlyByUserNotify);
                $window.plugins.OneSignal.sendTag(key+'_suport', user.profile.suport);
            });
            setVibrate(user.pushVibrate);
            setSound(user.pushSound);
        }

        function setSubscription(enabled) {
            if (!isAvailable()) return;
            $window.plugins.OneSignal.setSubscription(enabled);
        }

        function setSound(bool) {
            if (!isAvailable()) return;
            window.plugins.OneSignal.enableSound(bool);
        }

        function setVibrate(bool) {
            if (!isAvailable()) return;
            window.plugins.OneSignal.enableVibrate(bool);
        }

    }
})();
