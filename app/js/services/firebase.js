(function () {
    'user strict';

    angular.module('App')
        .factory('fb', function () {
            var config = {
                apiKey: "AIzaSyDapStKHfQZSAn0pz4rBJQj4kD4aMAnBXE",
                authDomain: "app-ja.firebaseapp.com",
                databaseURL: "https://app-ja.firebaseio.com",
                storageBucket: "app-ja.appspot.com",
                messagingSenderId: "33613406301"
            };
            if (firebase.apps.length === 0) {
                firebase.initializeApp(config);
            }
            return firebase.database().ref();
        });

})();