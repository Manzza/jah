(function () {
  'use strict';

  angular
    .module('App')
    .factory('pushServices', pushServices)
    .factory('retifyServices', retifyServices);

  // serviço de notificação
  pushServices.$inject = ['$http', '$q', 'firebaseService', '$rootScope'];
  function pushServices($http, $q, firebaseService, $rootScope) {
    var me = {};

    me.sendPush = function (message, title, sectionId, id, client) {
      var options = {
        client: client,
        msg: message,
        title: title,
        type: "OCORRENCIA",
        idOcor: id,
        idSection: sectionId
      }
      console.log(options);
      var NOTIFICATION_URL = 'http://52.67.191.118/sendToSegments_onlyByUserNotify';
      return $http.post(NOTIFICATION_URL, options).then(function (result) {
        console.log(result);
        return true;
      }).catch(function (error) {
        console.log(error);
        return alert('Falha no envio da Mensagem');

      });
    };

    me.sendChatPush = function (email, user, client) {
      var options = {
        email: email,
        client: client,
        msg: 'Você recebeu uma nova mensagem de ' + user.name,
        title: 'Chat',
        type: 'CHAT',
        from: user.$id
      }
      var NOTIFICATION_URL = 'http://52.67.191.118/sendPushByEmail';

      return $http.post(NOTIFICATION_URL, options).then(function (result) {
        return true;
      }).catch(function (error) {
        console.log(error);
        return alert('Falha no envio da Mensagem');
      });
    };

    me.sendSuportPush = function (user, client) {
      var options = {
        id: client,
        msg: 'Você recebeu uma nova mensagem de ' + user.name,
        title: 'Suporte ' + $rootScope.user.clients[$rootScope.currentGroup].nickname,
      }
      var NOTIFICATION_URL = 'http://52.67.191.118/sendToSegments_suport';
      return $http.post(NOTIFICATION_URL, options).then(function (result) {
        console.log(result);
        return true;
      }).catch(function (error) {
        console.log(error);
        return alert('Falha no envio da Mensagem');

      });
    };
    return me;
  }

  retifyServices.$inject = ['$http', '$q'];
  function retifyServices($http, $q) {
    var me = {};
    me.sendRetify = function (ocorId, msgAtu, userId) {
      var RETIFY_URL = 'http://ja.w3m.com.br/correcao_ocorrencia_app.php';
      var params = "?ocorId=" + ocorId + "&msgCor=" + msgAtu + "&userId=" + userId;
      return $http.post(RETIFY_URL + params).then(function (result) {
        return true;
      }).catch(function (error) {
        console.log(error);
        return alert('Falha no envio da correção');
      });
    };
    return me;
  }

})();