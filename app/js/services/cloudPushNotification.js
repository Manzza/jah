(function () {
  'use strict';

  angular
    .module('App')
    .factory('pushHandler', pushHandler);

  // serviço de notificação
  pushHandler.$inject = ['$http', '$q', '$state', '$rootScope', '$ionicLoading', 'firebaseService','$ionicHistory'];
  function pushHandler($http, $q, $state, $rootScope, $ionicLoading, firebaseService,$ionicHistory) {
    var me = {};

    me.cloudPushNotificantionHandler = function () {
      return $rootScope.$on('cloud:push:notification', function (event, data) {
        var msg = data.message;
        $rootScope.fromPush = true;
        $rootScope.currentGroup = msg.payload.client;
        $ionicLoading.show();
        firebaseService.emailLogin(window.localStorage.getItem('email'),
          window.localStorage.getItem('password')).then(function (result) {
            switch (msg.payload.type) {
              case "CHAT":
                $ionicLoading.hide();
                firebaseService.getUserById(msg.payload.from).then(function (result) {
                  $state.go('app.chat', { toUser: result });
                });
                break;
              case "SUPORT":
                $ionicLoading.hide();
                $state.go('app.chat', {groupId: { name: 'Suporte', guid:msg.payload.client + '_suporte' } });
                break;
              case "OCORRENCIA":
                firebaseService.getSectionById(msg.payload.section).then(function (section) {
                  $ionicLoading.hide();
                  $state.go('app.ocorrenciaFromPush', { id: msg.payload.id, section: section });
                });
                break;
            }
          }, function (error) {
            console.log(error);
            $ionicLoading.hide();

          });
      });
    };
    return me;
  }
})();