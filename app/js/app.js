// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'App' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

var app = angular.module('App', ['ionic', 'ionic-datepicker', 'ionic.cloud', 'ngAnimate', 'ngResource', 'ngCordova', 'ngCordovaOauth', 'ngTwitter', 'monospaced.elastic', 'angularMoment', 'firebase', 'filters', 'ngSanitize'])
    .run(['$ionicPlatform',
        'amMoment',
        'pushService',
        function ($ionicPlatform, amMoment,pushService) {
            $ionicPlatform.ready(function () {
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                    // for form inputs)
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                    StatusBar.overlaysWebView(false);
                    StatusBar.styleBlackTranslucent();
                    // Don't remove this line unless you know what you are doing. It stops the viewport
                    // from snapping when text inputs are focused. Ionic handles this internally for
                    // a much nicer keyboard experience.
                    cordova.plugins.Keyboard.disableScroll(true);
                    //js.src = "//connect.facebook.net/en_US/sdk.js";
                }
                if (pushService.isAvailable()) {
                    pushService.setup();
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
            });
        }])
    .config(['$stateProvider',
        '$urlRouterProvider',
        '$ionicConfigProvider',
        '$compileProvider',
        '$httpProvider',
        '$ionicCloudProvider',
        'ionicDatePickerProvider',
        
        function (
            $stateProvider,
            $urlRouterProvider,
            $ionicConfigProvider,
            $compileProvider,
            $httpProvider,
            $ionicCloudProvider,
            ionicDatePickerProvider) {

            $ionicConfigProvider.views.swipeBackEnabled(false);

            $ionicCloudProvider.init({
                "core": {
                    "app_id": "c9919fdd"
                },
                "push": {
                    "sender_id": "33613406301",
                    "pluginConfig": {
                        "ios": {
                            "badge": "true",
                            "sound": "true",
                            "alert": "true"
                        },
                        "android": {
                            "iconColor": "#ffffff"
                        }
                    }
                }
            });


            var datePickerObj = {
                inputDate: new Date(),
                titleLabel: 'Selecione uma data',
                setLabel: 'Definir',
                todayLabel: 'Hoje',
                closeLabel: 'Fechar',
                mondayFirst: false,
                weeksList: ["D", "S", "T", "Q", "Q", "S", "S"],
                monthsList: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dec"],
                templateType: 'popup',
                from: new Date(2017, 1, 1),
                to: new Date(2018, 12, 31),
                showTodayButton: false,
                dateFormat: 'dd MMMM yyyy',
                closeOnSelect: false,
                disableWeekdays: []
            };
            ionicDatePickerProvider.configDatePicker(datePickerObj);

            $ionicConfigProvider.navBar.alignTitle('center');

            $httpProvider.defaults.useXDomain = true;
            delete $httpProvider.defaults.headers.common['X-Requested-With'];
            
            $ionicConfigProvider.scrolling.jsScrolling(ionic.Platform.isIOS());

            $stateProvider
                .state('login', {
                    url: "/login",
                    templateUrl: "templates/login.html",
                    controller: 'LoginController'
                })
                .state('app.chat', {
                    url: "/chat",
                    views: {
                        viewContent: {
                            templateUrl: "templates/chat.html",
                            controller: 'ChatController'
                        }
                    },
                    params: {
                        toUser: null,
                        groupId: null,
                        suport: null
                    }

                })
                .state('app.facebook', {
                    url: "/facebook",
                    cache: true,
                    params: {
                        linkId: null
                    },
                    views: {
                        viewContent: {
                            templateUrl: "templates/facebook.html",
                            controller: 'FacebookController'
                        }
                    }
                })
                .state('app.config', {
                    url: "/config",
                    cache: true,
                    params: {
                        linkId: null
                    },
                    views: {
                        viewContent: {
                            templateUrl: "templates/config.html",
                            controller: 'ConfigController'
                        }
                    }
                })
                .state('app.twitter', {
                    url: "/twitter",
                    cache: true,
                    params: {
                        linkId: null
                    },
                    views: {
                        viewContent: {
                            templateUrl: "templates/twitter.html",
                            controller: 'TwitterController'
                        }
                    }
                })
                .state('app.contacts', {
                    url: "/contacts",
                    cache: true,
                    views: {
                        viewContent: {
                            templateUrl: "templates/contacts.html",
                            controller: 'ContactsController'
                        }
                    }
                }).state('app.home', {
                    url: "/home",
                    cache: false,
                    views: {
                        viewContent: {
                            templateUrl: "templates/home.html",
                            controller: 'HomeController'
                        }
                    },
                    params: {
                        active: 0,
                        filter: null,
                        filterOn: false
                    }
                })
                .state('app', {
                    url: '/app',
                    abstract: true,
                    controller: 'AppController',
                    templateUrl: 'templates/menu.html'
                })
                .state('app.ocorrenciaFromPush', {
                    url: "/item/:id",
                    params: {
                        id: null,
                        section: null

                    },
                    cache: false,
                    views: {
                        viewContent: {
                            templateUrl: "templates/ocorrenciaFromPush.html",
                            controller: 'ocorrenciaFromPushController'
                        }
                    }
                })
                .state('app.ocorrencia', {
                    url: "/item",
                    params: {
                        index: 0,
                        section: null,
                        active: 0
                    },
                    cache: false,
                    views: {
                        viewContent: {
                            templateUrl: "templates/ocorrencia.html",
                            controller: 'ocorrenciaController'
                        }
                    }
                });
            $urlRouterProvider.otherwise('login');

        }]);
app.value('_', window._);
