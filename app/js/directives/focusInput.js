angular.module('App').directive('focusInput', function($timeout) {
    return {
      link: function(scope, element, attrs) {
        element.bind('click', function() {
          $timeout(function() {
            element.parent().parent().find('input')[0].focus();
          });
        });
      }
    };
  });