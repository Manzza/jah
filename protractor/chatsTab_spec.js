describe('Testing the login', function() {
    it('should be able to input some data and click on button', function() {
        element(by.model('user.email')).sendKeys('rmanzini@interaxa.com');
        element(by.model('user.password')).sendKeys('123123');
        element(by.id('loginButton')).click();
        browser.sleep(5000)
        expect(element(by.model('choice')).isPresent()).toBe(true);
    });
});