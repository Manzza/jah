//Lista de import
var functions = require('firebase-functions');
var admin = require("firebase-admin");
var multer = require('multer');
var upload = multer();
const parseBody = multer().array();
var _ = require('underscore');
var $q = require('q');

admin.initializeApp({
  credential: admin.credential.cert({
    projectId: "app-ja",
    clientEmail: "firebase-adminsdk-4980u@app-ja.iam.gserviceaccount.com",
    privateKey: "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC7Hhptzl6KqZ35\njN6FN/F0rxGO4RwuxZDJO7QolZEdZ89gJMLneLpD6Z1YGNq5BvzOH620CH3fsBfD\nBSgXwvs0Mx1mKrChlcgcjUvftx9hQaaGpe4e32c0WXhfX/Q/071/dkTW8FewuiJ9\n/939gvNG+cgaHMw0hYZzT2WL/v9glVbS7eAT8xZNtYNPvm/n192M/TfxGWTIO+2q\njk3es3ClVMnMpA2cbrWEHpH802P4zw92SyaYeO9lxukjlU4ywrcT0tkl4FtZNb20\nB+dNLmKgeYx4L370XljKIPFtDjyTPNEsRCb2lIpbtNGoQGtk3C+Cmxid5bW8ysy5\ntF5mNmChAgMBAAECggEBAI8jyifx1JTSiEycnKTgVM9kFVRtlDebXtTx3TGmqOoL\nvbHYrViRHDxTe9AmwzsDw2STCzhLvq7dcG/5g+AZgj0+VOERCymxAPIGjOJInbKy\nnAco6+2mDLcZMmITby9cdPwKp0KdfRdubHBiyZjjEeNbtN105inPczjGpfHgUc+5\nXUCO65GmZTKcb8cmysb7J32f9dgRrNGcu7FQA1k70Ctr4oRtx/zavTYDMWiY5Cfb\nEPRPKDJAa7MzJMkJcuMmuw91m0URHCQ2H2zGhySyUh61Pjjokv4jQ1SBjOWsztoD\nQub6W6fROHRkJStm8pHYCeW8BR27emLZbhghHwKOl7kCgYEA9Qxjl5gFkrVDYCkw\nUNHXphNtpo+wCBXVQBaRKdBDxL2mNXarJrYx44PhSTMHE2r6dEjHxBcmfbMkVd30\n1VGcYEpgc1DiD4b9Y3ChmH5YsU5qGUubCQwL13fnit+tKrDeLiY0nmtYdBtp1PuW\nledz5AVAykpq1pG2A4DZ2Y6VbNsCgYEAw3rsyyQ+CUtKklPQi3QMn2UEC6/TnBm8\nGZtnmqjVxSzKYOdGq0O7Adp9z4jn0D0dJHBQP2BxxtepaFjtcfZk3BIq59h2tQZi\niFkpuhh4dRdm+kQbEuHYnsFSq3C+lY/My7hDv1A2Ia5Z82S8Oh7/rZKqvH2yKY4v\nfLtS7iLbYzMCgYBU5GXcLp4/pOwidCJ43TmIH1LtUPn60xV9jlPb2HqhvdHG08WX\nM6c02otBgJwC7hy2DECofhynj1gXJz2aaRtzgFDJokuudWVy/aH94kd3pWK8QKUF\nDVdM3g0o3zRuhbdy5Wl1OFr8XkLbtg0FAZn0dJ1tdzpr+y7DJ/BQnkTF4QKBgQCi\nvROliuaaljPe3igf8N0RVeeoUxi4PDR8nQQDnctZ+5AAXIpYympFgMB8pl1W34oD\na/1ixcfy4lgGpwIGfjo5fGZ73Zn12vrsyE4Uv4qI8JKqh0iCz8KnaAqK77G6kZzN\nfPnpq71lDOGV36dUO7pfplq90WchcV3XWZwQ5LQAQQKBgBfmZY9vSmdw+AuPvOpb\nalbZiSFNiLQEvvxtphup5yHrxr3cN7TpLe1TI5Vny/sai7e7Mmaj0Mt8C+9dpBip\nn7Ne+gBCpkZUyrLMsYF5Qu4aq2n1necHEzMrtJ7CcdBEZLOYcki7pciO0qvxnBPK\n1GERvTLwQjT2dy9h7x/o9Z3l\n-----END PRIVATE KEY-----\n"
  }),
  databaseURL: "https://app-ja.firebaseio.com"
});
var db = admin.database();

//Serviços
exports.status = functions.https.onRequest((request, response) => {
  response.status(200).send("Online");
});
exports.table_services = functions.https.onRequest((request, response) => {
  response.sendFile(__dirname + '/public/table_services.html');
});
exports.firebase_schema = functions.https.onRequest((request, response) => {
  response.sendFile(__dirname + '/public/firebase.html');
});
exports.cadastrarCliente = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var name = req.param('name', null);
    var thumb = req.param('thumb', null);
    var id = req.param('clientId', null)
    var nickname = req.param('nickname', null);
    var status = req.param('status', null) == "true";
    if (name && thumb && id && nickname && status != null) {
      var ref = db.ref("clients/" + id);
      ref.set({
        name: name,
        thumb: thumb,
        nickname: nickname,
        status: status
      }).then(function (group) {
        res.status(200).send("Sucesso");
      }).catch(function (error) {
        res.status(400).send(error + 'aqui');
      });
    } else {
      res.status(400).send("Erro no envia/leitura dos parametros");
    }
  })
})
exports.cadastrarPerfil = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var sup = req.param('suport', null);
    var chat = req.param('chat', null);
    var id = req.param('profileId', null)
    var not = req.param('notifies', null);
    var cor = req.param('rectify', null)
    if (sup != null && chat != null && id != null && not != null && cor != null) {
      var ref = db.ref("profiles/" + id);
      ref.set({
        suport: sup == "true",
        chat: chat == "true",
        notifies: not == "true",
        rectify: cor == "true"
      }).then(function () {
        res.status(200).send("Sucesso");
      }).catch(function (error) {
        res.status(400).send(error + 'aqui');
      });
    } else {
      res.status(400).send("Erro no envia/leitura dos parametros");
    }
  })
})
exports.cadastrarSecao = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var name = req.param('name', null);
    var banner = req.param('banner', null);
    var color = req.param('color', null);
    var status = req.param('status', null) == "true";
    var icon = req.param('icon', null);
    var id = req.param('sectionId', null);
    var clientList = req.param('clientList', null);
    clients = clientList != null ? clientList.split(",") : null;
    if (name != null && icon != null && id != null && banner != null && color != null && status != null) {
      var ref = db.ref("sections/" + id);
      ref.set({
        name: name,
        banner: banner,
        color: color,
        status: status,
        icon: icon,
        clients: clients
      }).then(function () {
        for (var i = 0; i < clients.length; i++) {
          var ref = db.ref("clients/" + clients[i] + "/sections/" + id);
          ref.set(true).then(function () {
          });
        }
        res.status(200).send("Sucesso");
      }).catch(function (error) {
        console.log(error);
        res.status(400).send(error + 'aqui');
      });
    } else {
      res.status(400).send("Erro no envia/leitura dos parametros");
    }
  })
})
exports.cadastrarUsuario = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var originalUser;
    var id = req.param('userId', null);
    var user = {};
    var creatEmail = req.param('email', null);
    var clientList = req.param('clientList', null);
    user.clients = clientList != null ? clientList.split(",") : null;
    user.email = creatEmail;
    user.name = req.param("displayName", null);
    user.thumb = req.param("photoURL", null);
    user.profile = req.param("profile", null);
    if (!checkUser(user, id)) {
      res.status(400).send("The read failed");
    } else {
      if (id == null) {
        admin.auth().createUser({
          email: creatEmail,
          emailVerified: false,
          password: "123213",
          displayName: user.displayName,
          photoURL: user.thumb,
          disabled: false
        }).then(function (userRecord) {
          var ref = db.ref("users/" + userRecord.uid);
          ref.set(user).then(function () {
            for (var i = 0; i < user.clients.length; i++) {
              var ref = db.ref("clients/" + user.clients[i] + "/users/" + userRecord.uid);
              ref.set(true).then(function () {
                res.status(200).send(userRecord.uid);
              });
            }
          });
        });
      } else {
        //remove o usuarios do cliente
        var ref = db.ref("users/" + id);
        ref.once("value", function (snapshot) {
          originalUser = snapshot.val();
          var keys = _.values(snapshot.val().clients);
          for (var i = 0; i < keys.length; i++) {
            var ref = db.ref("clients/" + keys[i] + "/users/" + id);
            ref.remove().then(function () {
            });
          }
        }).then(function () {
          var ref = db.ref("users/" + id);
          ref.set({
            clients: user.clients == null ? originalUser.clients : user.clients,
            email: originalUser.email,
            name: user.name == null ? originalUser.name : user.name,
            thumb: user.thumb == null ? originalUser.thumb : user.thumb,
            profile: user.profile == null ? originalUser.profile : user.profile,
          }).then(function () {
            var arr = user.clients == null ? originalUser.clients : user.clients;
            for (var i = 0; i < arr.length; i++) {
              var ref = db.ref("clients/" + arr[i] + "/users/" + id);
              ref.set(true).then(function () {
                res.status(200).send("Sucesso");
              })
            }
          })
        });
      }
    }
  });
});
/*exports.removerUsuario = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var originalUser;
    var id = req.param('userId', null);
    if (id == null) {
      res.status(400).send("The read failed");
    } else {
      //remove o usuarios do cliente
      var ref = db.ref("users/" + id);
      ref.once("value", function (snapshot) {
        originalUser = snapshot.val();
        var keys = _.values(snapshot.val().clients);
        for (var i = 0; i < keys.length; i++) {
          var ref = db.ref("clients/" + keys[i] + "/users/" + id);
          ref.remove().then(function () {
          });
        }
      }).then(function () {
        var ref = db.ref("users/" + id);
        ref.remove().then(function () {
          admin.auth().deleteUser(id)
            .then(function () {
              res.status(200).send("Sucesso");
            })
            .catch(function (error) {
              console.log("Error deleting user:", error);
            });
        })
      });
    }
  });
});
exports.removerSecao = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var originalSection;
    var id = req.param('sectionId', null);
    if (id == null) {
      res.status(400).send("The read failed");
    } else {
      //remove o usuarios do cliente
      var ref = db.ref("sections/" + id);
      ref.once("value", function (snapshot) {
        originalSection = snapshot.val();
        var keys = _.values(snapshot.val().clients);
        for (var i = 0; i < keys.length; i++) {
          var ref = db.ref("clients/" + keys[i] + "/sections/" + id);
          ref.remove().then(function () {
          });
        }
      }).then(function () {
        var ref = db.ref("sections/" + id);
        ref.remove().then(function () {
          res.status(200).send("Sucesso");
        }).catch(function (error) {
          console.log("Error deleting section:", error);
        });
      });
    }
  });
});
exports.removerCliente = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var originalClient;
    var id = req.param('clientId', null);
    if (id == null) {
      res.status(400).send("The read failed");
    } else {
      //remove o usuarios do cliente
      var ref = db.ref("clients/" + id);
      ref.once("value", function (snapshot) {
        originalClient = snapshot.val();
        var keys = snapshot.val().users;
        for (var index in keys) {
          var aux;
          var ref = db.ref("users/" + index + "/clients/");
          ref.once("value", function (snapshot) {
            aux = _.values(snapshot.val());
            aux.splice(aux.indexOf(id), 1);
            ref.set(aux).then(function () {
            })
          });
        }
      }).then(function () {
        var ref = db.ref("clients/" + id);
        ref.remove().then(function () {
          res.status(200).send("Sucesso");
        }).catch(function (error) {
          console.log("Error deleting client:", error);
        });
      });
    }
  });
});
exports.removerPerfil = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var arr = [];
    var id = req.param('profileId', null);
    function check(txt) {
      return txt == id;
    }
    if (id == null) {
      res.status(400).send("The read failed");
    } else {
      //remove o usuarios do cliente
      var ref = db.ref("users/");
      ref.once("value", function (snapshot) {
        snapshot.forEach(function (data) {
          arr.push(data.val().profile);
        });
      }).then(function () {
        if (_.uniq(arr).some(check)) {
          res.status(400).send("O perfil está em uso");
        } else {
          var ref = db.ref("profiles/" + id);
          ref.remove().then(function () {
            res.status(200).send("Sucesso");
          })
        }
      });
    }
  });
});
*/
exports.inserirLinkNoCliente = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var linkType = req.param('linkType', null);
    var linkId = req.param('linkId', null);
    var linkNickname = req.param('linkNickname', null);
    var id = req.param('clientId', null);
    if (id == null && linkId == null && linkNickname == null && linkType == null) {
      res.status(400).send("The read failed");
    } else {
      var ref = db.ref("clients/" + id + "/links");
      ref.push({ type: linkType, linkNickname: linkNickname, id: linkId }).then(function () {
        res.status(200).send("Sucesso");
      });
    }
  });
});
exports.alterarLinkNoCliente = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var originalLink = {};
    var link = {};
    link.id = req.param('linkId', null);
    link.type = req.param('linkType', null);
    link.nickname = req.param('linkNickname', null);
    var clientId = req.param('clientId', null);
    var linkId = req.param('linkKey', null);
    if (checkLink(link, clientId, linkId)) {
      var ref = db.ref("clients/" + clientId + "/links/" + linkId);
      ref.once("value", function (snapshot) {
        originalLink = snapshot.val();
      }).then(function () {
        var ref = db.ref("clients/" + clientId + "/links/" + linkId);
        ref.set({
          type: link.type == null ? originalLink.type : link.type,
          linkNickname: link.nickname == null ? originalLink.linkNickname : link.nickname,
          id: link.id == null ? originalLink.id : link.id
        }).then(function () {
          res.status(200).send("Sucesso");
        });
      })
    } else {
      res.status(400).send("The read failed");

    }
  });
});
exports.removerLinkNoCliente = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var client = req.param('clientId', null);
    var link = req.param('linkKey', null);
    if (linkId == null && clientId == null) {
      res.status(400).send("The read failed");
    } else {
      var ref = db.ref("clients/" + client + "/links/" + link);
      ref.remove().then(function (snapshot) {
        res.status(200).send("Sucesso");
      });
    }
  });
});
exports.alterarSecao = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var id = req.param('sectionId', null);
    var section = {};
    section.name = req.param('name', null);
    section.banner = req.param('banner', null);
    section.color = req.param('color', null);
    section.status = req.param('status', null) == "true";
    section.icon = req.param('icon', null);
    var clientList = req.param('clientList', null);
    section.clients = clientList != null ? clientList.split(",") : null;
    var originalSection = {};
    if (checkSection(section, id)) {
      var ref = db.ref("sections/" + id);
      ref.once("value", function (snapshot) {
        originalSection = snapshot.val();
        var keys = _.values(snapshot.val().clients);
        for (var i = 0; i < keys.length; i++) {
          var ref = db.ref("clients/" + keys[i] + "/sections/" + id);
          ref.remove().then(function () {
          });
        }
      }).then(function () {
        var ref = db.ref("sections/" + id);
        ref.set({
          name: section.name == null ? originalSection.name : section.name,
          banner: section.banner == null ? originalSection.banner : section.banner,
          color: section.color == null ? originalSection.color : section.color,
          status: section.status == null ? originalSection.status : section.status,
          icon: section.icon == null ? originalSection.icon : section.icon,
          clients: section.clients == null ? originalSection.clients : section.clients,
        }).then(function () {
          var arr = section.clients == null ? originalSection.clients : section.clients;
          for (var i = 0; i < arr.length; i++) {
            var ref = db.ref("clients/" + arr[i] + "/sections/" + id);
            ref.set(true).then(function () {
            });
          }
          res.status(200).send("Sucesso");
        }).catch(function (error) {
          res.status(400).send(error + 'aqui');
        });
      })
    } else {
      res.status(400).send("Erro no envia/leitura dos parametros");
    }
  })
})
exports.alterarCliente = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var client = {}
    var id = req.param('clientId', null)
    client.name = req.param('name', null);
    client.thumb = req.param('thumb', null);
    client.nickname = req.param('nickname', null);
    clientstatus = req.param('status', null) == "true";
    var originalClient = {};
    if (checkClient(client, id)) {
      var ref = db.ref("clients/" + id);
      ref.once("value", function (snapshot) {
        originalClient = snapshot.val();
      }).then(function () {
        var ref = db.ref("clients/" + id);
        ref.set({
          name: client.name == null ? originalClient.name : client.name,
          thumb: client.thumb == null ? originalClient.thumb : client.thumb,
          nickname: client.nickname == null ? originalClient.nickname : client.nickname,
          status: client.status == null ? originalClient.status : client.status,
          links: originalClient.links == null ? null : originalClient.links,
          sections: originalClient.scetions == null ? null : originalClient.scetions,
          users: originalClient.users == null ? null : originalClient.users
        }).then(function () {
          res.status(200).send("Sucesso");
        }).catch(function (error) {
          res.status(400).send(error + 'aqui');
        });
      })
    } else {
      res.status(400).send("Erro no envia/leitura dos parametros");
    }
  })
})
exports.alterarPerfil = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var profile = {};
    var id = req.param('profileId', null)
    profile.suport = req.param('suport', null) == "true";
    profile.chat = req.param('chat', null) == "true";
    profile.notifies = req.param('notifies', null) == "true";
    profile.rectify = req.param('rectify', null) == "true";
    var originalProfile = {};
    if (checkProfile(profile, id)) {
      var ref = db.ref("profiles/" + id);
      ref.once("value", function (snapshot) {
        originalProfile = snapshot.val();
      }).then(function () {
        var ref = db.ref("profiles/" + id);
        ref.set({
          suport: profile.suport == null ? originalProfile.suport : profile.suport,
          chat: profile.chat == null ? originalProfile.chat : profile.chat,
          notifies: profile.notifies == null ? originalProfile.notifies : profile.notifies,
          rectify: profile.rectify == null ? originalProfile.rectify : profile.rectify
        }).then(function () {
          res.status(200).send("Sucesso");
        }).catch(function (error) {
          res.status(400).send(error + 'aqui');
        });
      })

    } else {
      res.status(400).send("Erro no envia/leitura dos parametros");
    }
  })
})
exports.getAllFromList = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var listId = req.param('listId', null);
    if (listId == null) {
      res.status(400).send("Erro no envia/leitura dos parametros");
    } else {
      var Ref = db.ref(listId + '/');
      Ref.once("value", function (snapshot) {
        if (snapshot.exists()) {
          res.status(200).send(snapshot.val());
        } else {
          res.status(200).send('{}');
        }
      }, function (errorObject) {
        res.status(400).send("The read failed: " + errorObject.code);
      });
    }
  });
});
exports.getItemFromList = functions.https.onRequest((req, res) => {
  parseBody(req, res, function (err) {
    var listId = req.param('listId', null);
    var itemId = req.param('itemId', null);
    if (listId == null && itemId == null) {
      res.status(400).send("The read failed");
    } else {
      var Ref = db.ref(listId + "/" + itemId);
      Ref.once("value", function (snapshot) {
        if (snapshot.exists()) {
          res.status(200).send(snapshot.val());
        } else {
          res.status(200).send('{}');
        }
      }, function (errorObject) {
        res.status(400).send("The read failed: " + errorObject.code);
      });
    }
  })
});
function checkUser(user, id) {
  if (id == null) {
    return user.clients != null && user.name != null && user.thumb != null && user.email != null;
  } else {
    return user.clients != null || user.name != null || user.thumb != null || user.profile != null || user.email != null;
  }
};
function checkProfile(profile, id) {
  return id != null && (profile.suport != null || profile.chat != null || profile.notifies != null || profile.rectify != null);
};
function checkSection(section, id) {
  return id != null && (section.clients != null || section.name != null || section.banner != null || section.color != null || section.status != null || section.icon != null);
};
function checkClient(client, id) {
  return id != null && (client.name != null || client.thumb != null || client.status != null || client.nickname != null);
};
function checkLink(link, id, id2) {
  return id != null && id2 != null && (link.type != null || link.id != null || link.nickname != null);
};